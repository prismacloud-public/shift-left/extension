#/bin/sh
if ! /tmp/twistcli --version 2> /dev/null; then
  echo "Download twistcli binary file ..."
  curl -k -u ${prisma_cloud_compute_username}:${prisma_cloud_compute_password} \
    --output /tmp/twistcli ${prisma_cloud_compute_url}/api/v1/util/twistcli
  chmod +x /tmp/twistcli
fi
/tmp/twistcli --version
set +e
if [ ! -z \"$prisma_cloud_compute_project\" ]; 
  then twistproj=(--project=\"$prisma_cloud_compute_project\"); 
fi
/tmp/twistcli images scan \"${twistproj[@]}\" --details \
  --address $prisma_cloud_compute_url \
  --user=$prisma_cloud_compute_username \
  --password=$prisma_cloud_compute_password \
  --output-file twistcli.json $prisma_cloud_scan_image
rc=$?
if [ -f twistcli.json -a ! -z "${prisma_cloud_compute_report_dir}" ]; then
  prisma_cloud_compute_report_dir=$(echo $prisma_cloud_compute_report_dir | sed 's:/*$::' | sed 's:^/*::')
  mkdir -p ${prisma_cloud_compute_report_dir}
  touch ${prisma_cloud_compute_report_dir}/results.xml
  docker run --rm \
    -v $PWD/twistcli.json:/tmp/twistcli.json \
    -v $PWD/${prisma_cloud_compute_report_dir}/results.xml:/tmp/results.xml \
    redlock/pcs-sl-scanner pcs_compute_junit_report
fi
exit $rc
